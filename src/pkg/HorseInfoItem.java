package pkg;

public class HorseInfoItem {
	private int horse_no;
	private String horse;
	private String jockey;
	private String trainer;
	private int draw;
	private int wgt;
	private int act_wgt;
	private int body_wgt;
	private int rtg;
	private String gear;
	private String attend;
	private int[] last_6_run;
	
	public HorseInfoItem(int hor_no, String hor, int dra, int wt, int act_wt, String joc, String tra, int body_wt, int rt,
			String gea, int[] last_6_run, String attend) {
		this.horse_no = hor_no;
		this.horse = hor;
		this.jockey = joc;
		this.trainer = tra;
		this.draw = dra;
		this.wgt = wt; 
		this.act_wgt = act_wt;
		this.body_wgt = body_wt;
		this.rtg = rt;
		this.gear = gea;
		this.last_6_run = last_6_run;
		this.attend = attend;
	}
	
	void setHorse(String horse) {
		this.horse = horse;
	}
	
	void setJockey(String jockey) {
		this.jockey = jockey;
	}
	
	void setTrainer(String trainer) {
		this.trainer = trainer;
	}
	
	void setGear(String gear) {
		this.gear = gear;
	}
	
	void setRtg(int rtg) {
		this.rtg = rtg;
	}
	
	void setLast6Run(int[] last_6_run) {
		this.last_6_run = last_6_run;
	}
	
	void setDraw(int draw) {
		this.draw = draw;		
	}
	
	void setWt(int wt) {
		this.wgt = wt;
	}
	
	void setActWt(int act_wt) {
		this.act_wgt = act_wt;
	}
	
	
	void setBodyWt(int body_wt) {
		this.body_wgt = body_wt;
	}
	

	void setAttend(String attend) {
		this.attend = attend;
	}
	
	int getHorseNo() {
		return horse_no;
	}
	
	String getHorse() {
		return horse;
	}

	String getJockey() {
		return jockey;
	}

	String getTrainer() {
		return trainer;
	}

	int getDraw() {
		return draw;
	}

	int getWgt() {
		return wgt;
	}
	
	int getActWgt() {
		return act_wgt;
	}

	int getBodyWgt() {
		return body_wgt;
	}

	String getGear() {
		return gear;
	}

	int getRtg() {
		return rtg;
	}
	
	int[] getLast6Run() {
		return last_6_run;
	}
	
	String getAttend() {
		return attend;
	}
}
