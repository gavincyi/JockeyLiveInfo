package pkg;
import java.util.List;

public class RaceInfoItem {
	private int race;
	private String race_date;
	private String race_time;
	private String race_class;
	private String track;
	private String course;
	private int dist;
	private String going;
	private int total_horses;
	private List<HorseInfoItem> horse_arr;
	
	public RaceInfoItem(int race, String race_date, String race_time, String race_class, String track, String course, int dist, String going) {
		this.race = race;
		this.race_date = race_date;
		this.race_time = race_time;
		this.race_class = race_class;
		this.track = track;
		this.course = course;
		this.dist = dist;
		this.going = going;
	}
	
	public void setTime(String race_time) {
		this.race_time = race_time;
	}
	
	public void setHorse(int total_horses, List<HorseInfoItem> horse_arr) {
		this.total_horses = total_horses;
		this.horse_arr = horse_arr;
	}
	
	int getRaceNo() {
		return this.race;
	}
	
	String getRaceDate() {
		return this.race_date;
	}
	String getRaceTime() {
		return this.race_time;
	}
	
	String getClassNo() {
		return this.race_class;
	}
	
	String getTrack() {
		return this.track;
	}
	
	String getCourse() {
		return this.course;
	}

	int getDist() {
		return this.dist;
	}
	
	String getGoing() {
		return this.going;
	}
	
	int getTotalHorses() {
		return this.total_horses;
	}
	
	List<HorseInfoItem> getHorseArr() {
		return this.horse_arr;
	}
}
