package pkg;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class InvProc implements InProc {

	private static Logger logger_ = Logger.getLogger(InvProc.class);

	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name,
			int last_time) {

		if (inputLine.contains("INV")) {
			Map<String, Integer> total_inv = new HashMap<String, Integer>();

			String time = inputLine.substring(inputLine.indexOf("Time") + 6, inputLine.indexOf("\"><RACE"));
			int upt_time = Integer.parseInt(time.substring(0, 2) + time.substring(3, 5) + time.substring(6));
			if (upt_time > last_time || last_time - upt_time > 120000) {

				last_time = upt_time;

				int cnt = 0;
				String input_arr[] = inputLine.substring(inputLine.indexOf("<INV"), inputLine.lastIndexOf("</RACE>"))
						.split("</INV>");
				for (int i = 0; i < input_arr.length; i++) {
					int turnover = Integer.parseInt(input_arr[i].substring(input_arr[i].indexOf(">") + 1));
					String type = null;
					if (input_arr[i].contains("\"\"")) {
						if (cnt == 0) {
							type = "FSTQTT";
							cnt += 1;
						} else {
							type = "WIN";
							cnt += 1;
						}
					} else {
						type = input_arr[i].substring(input_arr[i].indexOf("POOL") + 6,
								input_arr[i].indexOf("\" TIME"));
					}
					total_inv.put(type, turnover);
					GlobalVar.INST.turnover.put(race + type, turnover);
				}

				String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
				String sql = "insert into " + tb_name + " values(NULL, \"" + datetime_str.split(" ")[0] + "\",\""
						+ datetime_str.split(" ")[1] + "\",\"" + time + "\"," + race + ",\"" + race_time + "\","
						+ total_inv.get("WIN") + "," + total_inv.get("PLA") + "," + total_inv.get("QIN") + ","
						+ total_inv.get("QPL") + "," + total_inv.get("TCE") + "," + total_inv.get("TRI") + ","
						+ total_inv.get("FSTQTT") + "," + total_inv.get("DBL") + ")";

				try {
					DbUtil.INST.exec(sql);
				} catch (Exception ex) {
					logger_.error("Exception: ", ex);
				}
			}
		}
		return last_time;
	}
}
