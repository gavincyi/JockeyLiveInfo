package pkg;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class UptRaceTime implements InProc {

	private static Logger logger_ = Logger.getLogger(UptRaceTime.class);
	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name, int last_time) {
		
		if (inputLine.contains("class=\"content\" nowrap")) {
			String arr[] = inputLine.split("</nobr>");
			String new_race_time = arr[1].substring(arr[1].indexOf(">") + 1);
			if (!new_race_time.equals(race_time)) {
				logger_.info("Change the race time for race " + race + " from " + race_time + " to " + new_race_time);
				String sql = "UPDATE `" + tb_name + "` set race_time = \"" + new_race_time + "\" where race = " + race;
				try {
					DbUtil.INST.exec(sql);
				} catch (Exception ex) {
					logger_.error("Exception: ", ex);
				}
				
				race_info.setTime(new_race_time);
			}
		}
		
		
		if(inputLine.contains("tableContent2")) {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm");
			//Date curr_time = new Date();
			//Date start_time = new Date();
			Calendar cal = Calendar.getInstance(); 
			cal.add(Calendar.MINUTE, -3);
			Date curr_time = cal.getTime();
			Date start_time = new Date();
			try {
				start_time = dateFormat.parse(race_info.getRaceDate() + " " + race_time);
			} catch (ParseException e) {
				logger_.error("ParseException: ", e);
			}
			
			if(curr_time.before(start_time)) {
				
				String arr[] = inputLine.split("</tr>");
				int total_horses = arr.length;
				String[][] recs = new String[total_horses][12];
				int[] wt_increment = new int[total_horses];
				String[] attend_arr = new String[total_horses];
				int[][] last_6_run_arr = new int[total_horses][6];
				
				for(int i = 0; i < total_horses; i++) {
					
					if(arr[i].contains("SCR") && race_info.getHorseArr().get(i).getAttend().equals("Y")) {
						logger_.info("Change the attend state of horse " + (i+1) + " in race " + race + " to 'N'");
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						race_info.getHorseArr().get(i).setAttend("N");
						String sql =  "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", attend = 'N' where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
										
					
					String cols[] = arr[i].split("</td>");
					
					if(cols.length < 10) {
						for(int j = 1; j < cols.length -1; j++) {
							recs[i][j] = cols[j].substring(cols[j].lastIndexOf("\">")+2);
							if(j == 3) 
								recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf("<"));									
						}
						for(int j = 4; j < 12; j++)
							recs[i][j] = "0";
						attend_arr[i] = "N";
						
					} else {
						
						for(int j = 1; j < cols.length; j++) {										
							recs[i][j] = cols[j].substring(cols[j].lastIndexOf("\">")+2);
							if(j == 6 && recs[i][j].contains("(")) {
								//wt_increment[i] = Integer.parseInt(recs[i][j].substring(recs[i][j].indexOf("(")+1, recs[i][j].indexOf(")")).replace("?", ""));
								String wt_change = recs[i][j].substring(recs[i][j].indexOf("(")+1, recs[i][j].indexOf(")")).replace("?", "");
								if(wt_change.matches("^[+-]?\\d+$")) {
									wt_increment[i] = Integer.parseInt(wt_change);
								}
							} else if(j == 6)
								wt_increment[i] = 0;
							if(j == 3 || j == 6 || j == 7) {
								recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf("<"));
							}
							
							if(j == 6 && recs[i][j].contains("(")) {
								recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf(" ("));
							}
							
							if(j == 8) {
								recs[i][j] = recs[i][j].replace("&nbsp;", "0");
							}
							
							if(j == 9 && !recs[i][j].matches("[0-9]+"))
								recs[i][j] = "0";
							
							if(j == 10) {			
								if(recs[i][j].contains("&nbsp;"))
									recs[i][j] = "";
								else
									recs[i][j] = recs[i][j].substring(recs[i][j].indexOf(">")+1, recs[i][j].indexOf("</"));
							}
							
							if(j == 11) {
								String[] runs = recs[i][j].split("/"); 
								int k;
								for(k = 0; k < runs.length; k++) {
									if(runs[k].contains("&nbsp;") || runs[k].contains("-"))
										last_6_run_arr[i][k] = 0;
									else
										last_6_run_arr[i][k] = Integer.parseInt(runs[k]);
								}
								
								for(; k < 6; k++) {
									last_6_run_arr[i][k] = 0;
								}
							}
						}
						attend_arr[i] = "Y";
					}
				}
			
			
				for (int i = 0; i < total_horses; i++) {
					HorseInfoItem horse_info = race_info.getHorseArr().get(i);
					
					String temp_horse = recs[i][3];
					String pre_horse = horse_info.getHorse();
					if(!temp_horse.equals(pre_horse)) {
						logger_.info("Change the name of horse " + (i+1) + " in race " + race + " from " + pre_horse + " to " + temp_horse);
						horse_info.setHorse(temp_horse);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", horse = \"" + temp_horse + "\" where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					
					int temp_draw = Integer.parseInt(recs[i][4]);
					int pre_draw = horse_info.getDraw();
					if( pre_draw!= temp_draw) {
						logger_.info("Change the draw of horse " + (i+1) + " in race " + race + " from " + pre_draw + " to " + temp_draw);
						horse_info.setDraw(temp_draw);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", draw = " + temp_draw + " where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					int temp_wgt = Integer.parseInt(recs[i][5]);
					int pre_wgt = horse_info.getWgt();
					if( pre_wgt!= temp_wgt) {
						logger_.info("Change the wgt of horse " + (i+1) + " in race " + race + " from " + pre_wgt + " to " + temp_wgt);
						horse_info.setWt(temp_wgt);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", wt = " + temp_wgt + " where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					
					temp_wgt = Integer.parseInt(recs[i][5]) + wt_increment[i];
					pre_wgt = horse_info.getActWgt();
					if( pre_wgt!= temp_wgt) {
						logger_.info("Change the actual wgt of horse " + (i+1) + " in race " + race + " from " + pre_wgt + " to " + temp_wgt);
						horse_info.setActWt(temp_wgt);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", act_wt = " + temp_wgt + " where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					String temp_joc = recs[i][6];
					String pre_joc = horse_info.getJockey();
					if(!temp_joc.equals(pre_joc)) {
						logger_.info("Change the jockey of horse " + (i+1) + " in race " + race + " from " + pre_joc + " to " + temp_joc);
						horse_info.setJockey(temp_joc);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", jockey = \"" + temp_joc + "\" where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					String temp_trainer = recs[i][7];
					String pre_trainer = horse_info.getTrainer();
					if(!temp_trainer.equals(pre_trainer)) {
						logger_.info("Change the trainer of horse " + (i+1) + " in race " + race + " from " + pre_trainer + " to " + temp_trainer);
						horse_info.setTrainer(temp_trainer);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", trainer = \"" + temp_trainer + "\" where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					int temp_body_wgt = Integer.parseInt(recs[i][8]);
					int pre_body_wgt = horse_info.getBodyWgt();
					if( pre_body_wgt!= temp_body_wgt) {
						logger_.info("Change the body wgt of horse " + (i+1) + " in race " + race + " from " + pre_body_wgt + " to " + temp_body_wgt);
						horse_info.setBodyWt(temp_body_wgt);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", body_wt = " + temp_body_wgt + " where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					
					int temp_rtg = Integer.parseInt(recs[i][9]);
					int pre_rtg = horse_info.getRtg();
					if( pre_rtg!= temp_rtg) {
						logger_.info("Change the rtg of horse " + (i+1) + " in race " + race + " from " + pre_rtg + " to " + temp_rtg);
						horse_info.setRtg(temp_rtg);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", rtg = " + temp_rtg + " where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					String temp_gear = recs[i][10];
					String pre_gear = horse_info.getGear();
					if(!temp_gear.equals(pre_gear)) {
						logger_.info("Change the gear of horse " + (i+1) + " in race " + race + " from " + pre_gear + " to " + temp_gear);
						horse_info.setGear(temp_gear);
						String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
						String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
								+ "\", gear = \"" + temp_gear + "\" where race = " + race + " and horse_no = " + (i+1);
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
					}
					
					int[] pre_runs = horse_info.getLast6Run();
					for(int j = 0; j < 6; j++) {
						if(last_6_run_arr[i][j] != pre_runs[j]) {
							logger_.info("Change the last 6 runs of horse " + (i+1) + " in race " + race);
							horse_info.setLast6Run(last_6_run_arr[i]);
							String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
							String sql = "UPDATE `" + tb_name + "` set date = \"" + datetime_str.split(" ")[0] + "\", time = \"" + datetime_str.split(" ")[1] 
									+ "\", ";
							for(int k = 0; k < 6; k++) {
								sql += "last_run" + (k+1) + " = " + last_6_run_arr[i][k];
								if(k < 5) {
									sql += ", ";
								}
							}
							sql += " where race = " + race + " and horse_no = " + (i+1);
							try {
								DbUtil.INST.exec(sql);
							} catch (Exception ex) {
								logger_.error("Exception: ", ex);
							}
							break;
						}
					}
					
					
				}
			}
		}
					
		return last_time;
	}

}
