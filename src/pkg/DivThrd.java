package pkg;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

public class DivThrd extends Thread {
	private RaceInfoItem[] race_info_arr;
	private String race_date;
	private int load_wait_time;
	private int thrd_wait_time;
	private String tb_name;
	private String thrd_name;
	private String venue;
	
	private static Logger logger_ = Logger.getLogger(DivThrd.class);
	DivThrd(RaceInfoItem[] race_info_arr, int load_wait_time, int thrd_wait_time, String tb_name, String race_date, String thrd_name, String venue){
		this.race_info_arr = race_info_arr;
		this.race_date = race_date;
		this.load_wait_time = load_wait_time;
		this.thrd_wait_time = thrd_wait_time;
		this.tb_name = tb_name;
		this.thrd_name = thrd_name;
		this.venue = venue;
		logger_.info("Creating " + thrd_name + " thread");
	}
	
	public void run() {

		logger_.info("Running " + thrd_name + " thread");
		
		while (true) {				
			
			try {
				Date today = new Date();
				Date race1_date = new SimpleDateFormat("yyyyMMdd HH:mm").parse(race_date + " " + race_info_arr[0].getRaceTime());
				long diff = race1_date.getTime() - today.getTime() + 20*60*1000;
				if(diff > 0)
					Thread.sleep(diff);
				
				int total_race = race_info_arr.length;
				for(int race = 1; race <= total_race;) {					
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.MINUTE, -20);
					Date curr = calendar.getTime();
					String ori_time = race_info_arr[race-1].getRaceTime();
					Date dt = new SimpleDateFormat("yyyyMMdd HH:mm").parse(race_date + " " + ori_time);
					if (curr.after(dt)) {
						boolean res;
						if(tb_name.contains("FR"))
							res = update_overseas_div_result(race_date, venue, load_wait_time, race, tb_name);
						else
							res = update_div_result(race_date, venue, load_wait_time, race, tb_name);
						if(res == true) {
							++race;
						}
					} else {
						//System.out.println(dt.getTime() - curr.getTime());
						Thread.sleep(dt.getTime() - curr.getTime());
					}
				}
				
				break;
								
			} catch (Exception e) {					
				logger_.error("Exception:", e);
				try {
					Thread.sleep(thrd_wait_time);
				} catch (InterruptedException ie) {
					logger_.error(thrd_name + " thread interrupted.", ie);
				}
			} 					
	
			try {
				Thread.sleep(thrd_wait_time);
			} catch (InterruptedException e) {
				logger_.error( thrd_name + "thread interrupted.", e);
			}
		}
		
		logger_.info(thrd_name + " thread exiting.");
	}
	
	
	public static boolean update_div_result(String race_date, String venue, int load_wait_time, int race, String tb_name) {
		logger_.info("update dividend result for race " + race);
		boolean res = false;
		try {	
			boolean	loading = false;
			do {
				String url_addr = "http://racing.hkjc.com/racing/Info/Meeting/Results/English/Local/" + race_date
						+ "/" + venue + "/" + race;
				logger_.info(url_addr);
				
				loading = false;
				String sql = null;
				
				List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}
	
					if (inputLine.contains("Dividend&nbsp;(HK$)")) {
						inputLine = StringEscapeUtils.unescapeHtml4(inputLine);
						inputLine = inputLine.substring(inputLine.indexOf("(HK$)") + 15,
									inputLine.indexOf("</table>"));
						String row[] = inputLine.split("</tr>");
						String temp_pool = null;
						String temp_com = null;
						String temp_div = null;
	
						sql = "insert into " + tb_name + " values ";
						for (int i = 0; i < row.length; i++) {
							if (row[i].contains("Detail") && !row[i].contains("JOCKEY"))
								continue;
							String col[] = row[i].split("</td>");
							for (int j = 0; j < col.length; j++) {
								if(col[j].contains("Details")) {
									temp_div = "0";
								} else if (col[j].contains("rowspan")) {
									temp_pool = col[j].substring(col[j].lastIndexOf("\">") + 2);
									if (temp_pool.contains("<"))
										temp_pool = temp_pool.substring(0, temp_pool.indexOf("<"));
								} else if (col[j].contains("tdAlignR")) {
									temp_div = col[j].substring(col[j].lastIndexOf(">") + 1);
									temp_div = temp_div.replace(",", "");
									if (temp_div.contains("NOT"))
										temp_div = "0";
									if (temp_div.contains("$"))
										temp_div = Double.toString(
													Double.parseDouble(temp_div.substring(0, temp_div.indexOf("/")))
														* Double.parseDouble(
																temp_div.substring(temp_div.indexOf("$") + 1))
												/ 10);
	
								} else if (row[i].contains("JOCKEY") && j == 0) {
									temp_pool = col[j].substring(col[j].lastIndexOf(">")+1);
								} else if (j == 1 && col[j].contains("<br />")) {
									col[j].replace("<br />", " ");
									temp_com = col[j].substring(col[j].lastIndexOf(">") + 1);
								} else {
									temp_com = col[j].substring(col[j].lastIndexOf(">") + 1);
								}
							}
	
							sql += "(NULL, \"" + venue + "\"," + race + ",\"" + temp_pool + "\",\""
									+ temp_com + "\"," + temp_div + ")";
														
							sql += ",";

						}
						sql = sql.substring(0, sql.length()-1) + ";";
						DbUtil.INST.exec(sql);
						res = true;
					}
				}
	
			} while (loading == true);
		} catch (IOException e) {
			logger_.error("IOException: ", e);	
		} catch (Exception e) {
			logger_.error("Exception: ", e);
		}
		
		logger_.info("Finish getting result for race " + race);
		return res;
	}
	
	
	/*
	public static boolean update_overseas_div_result(String race_date, String venue, int load_wait_time, int race, String tb_name) {
		logger_.info("update overseas dividend result for race " + race);
		boolean res = false;
		try {
			boolean	loading = false;
			do {							            
				String url_addr = "http://racing.hkjc.com/racing/info/meeting/ResultsAll/English/Local/" + race_date;
				logger_.info(url_addr);
				//BufferedReader in = URLConn.url_conn(url_addr, load_wait_time);
				//String inputLine;
				List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
				loading = false;
				String sql = null;
				
				//while ((inputLine = in.readLine()) != null) {
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}
	
					if(inputLine.contains("Overseas ")) {
						String pre_venue = inputLine.substring(inputLine.indexOf("(")+1, inputLine.indexOf(")"));
						int temp_race = Integer.parseInt(inputLine.substring(inputLine.indexOf("Race ")+5, inputLine.indexOf("</div>")));
						if(temp_race ==  race && pre_venue.equals(venue)) {
							inputLine = inputLine.substring(inputLine.indexOf("<table"), inputLine.indexOf("</table>"));
							String row[] = inputLine.split("</tr>");
							String temp_pool = null;
							String temp_com = null;
							String temp_div = null;
							for (int i = 2; i < row.length; i++) {
								String cols[] = row[i].split("</td>");
								for(int j = 0; j < cols.length; j++) {
									if(cols[j].contains("rowspan")) {
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+1);
									} else if(cols[j].contains("tdAlignR")) {
										temp_div = cols[j].substring(cols[j].lastIndexOf(">")+1); 
										temp_div = temp_div.replace(",", "");
										if (temp_div.contains("NOT"))
											temp_div = "0";
										if (temp_div.contains("$"))
											temp_div = Double.toString(
														Double.parseDouble(temp_div.substring(0, temp_div.indexOf("/")))
															* Double.parseDouble(
																	temp_div.substring(temp_div.indexOf("$") + 1))
													/ 10);
									} else {
										temp_com = cols[j].substring(cols[j].lastIndexOf(">")+1);
									}
									
								}
								if(sql == null)
									sql = "insert into " + tb_name + " values ";
								sql += "(NULL, \"" + pre_venue + "\"," + race + ",\"" + temp_pool + "\",\""
										+ temp_com + "\"," + temp_div + ")";
								sql += ",";
							}
							
							break;
						}
					}
				}
				
				if(sql != null) {
					sql = sql.substring(0, sql.length()-1) + ";";
					DbUtil.INST.exec(sql);
					res = true;					
				}
				//in.close();
				
			} while (loading == true);
			
		} catch (Exception e) {
			logger_.error("Exception: ", e);	
		}
		logger_.info("Finish getting result for race " + race);
		return res;
	}
	*/

	
	public static boolean update_overseas_div_result(String race_date, String venue, int load_wait_time, int race, String tb_name) {
		logger_.info("update overseas dividend result for race " + race);
		boolean res = false;
		try {
			boolean	loading = false;
			do {							            
				//String url_addr = "http://racing.hkjc.com/racing/info/meeting/ResultsAll/English/Local/" + race_date;
				String url_addr = "http://racing.hkjc.com/racing/SystemDataPage/racing/ResultsAll-iframe-SystemDataPage.aspx?match_id=" + race_date + "&lang=English";
				logger_.info(url_addr);
				List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
				loading = false;
				String sql = null;
				int state = 0;
				
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}
					
					if(state == 2)
						break;
	
					if(state == 0 && inputLine.contains("Overseas ") ) {
						String pre_venue = inputLine.substring(inputLine.indexOf("(")+1, inputLine.indexOf(")"));
						int temp_race = Integer.parseInt(inputLine.substring(inputLine.indexOf("Race ")+5, inputLine.indexOf(" </div>")));
						if( temp_race ==  race && pre_venue.equals(venue))
							state = 1;
					} else if(state == 1 && inputLine.contains("REFUND")) {
						String cols[] = inputLine.split("</td>");
						String temp_pool = cols[0].substring(cols[0].indexOf(">")+2);
						String temp_com = cols[1].substring(cols[0].indexOf(">")+1);
						String temp_div = "0";
						if(sql == null)
							sql = "insert into " + tb_name + " values ";
						sql += "(NULL, \"" + venue + "\"," + race + ",\"" + temp_pool + "\",\""
								+ temp_com + "\"," + temp_div + "), ";
					} else if(state == 1 && (inputLine.contains("WIN") || inputLine.contains("</table>"))) {
						
						inputLine = inputLine.substring(inputLine.indexOf("<tr"));
						if(inputLine.contains("</table")) {
							inputLine = inputLine.substring(0, inputLine.indexOf("</table>"));
							state = 2;
						} else {
							inputLine = inputLine.substring(0, inputLine.lastIndexOf("</tr>"));
						}
						
						String row[] = inputLine.split("</tr>");
						String temp_pool = null;
						String temp_com = null;
						String temp_div = null;
						for (int i = 0; i < row.length; i++) {
							String cols[] = row[i].split("</td>");
							for(int j = 0; j < cols.length; j++) {
								if(cols[j].contains("rowspan")) {
									if(cols[j].contains("</br>")) {
										cols[j] = cols[j].substring(0, cols[j].indexOf("</br>")) + cols[j].substring(cols[j].indexOf("</br>")+5);
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+2);
									} else if (cols[j].contains("<BR/>")) {
										cols[j] = cols[j].substring(0, cols[j].indexOf("<BR/>")) + " " + cols[j].substring(cols[j].indexOf("<BR/>")+6);
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+1);
									} else {
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+2);
									}
								} else if(cols[j].contains("tdAlignR")) {
									temp_div = cols[j].substring(cols[j].lastIndexOf(">")+1); 
									temp_div = temp_div.replace(",", "");
									if (temp_div.contains("NOT"))
										temp_div = "0";
									if (temp_div.contains("$"))
										temp_div = Double.toString(
													Double.parseDouble(temp_div.substring(0, temp_div.indexOf("/")))
														* Double.parseDouble(
																temp_div.substring(temp_div.indexOf("$") + 1))
												/ 10);
								} else {
									temp_com = cols[j].substring(cols[j].lastIndexOf(">")+1);
								}
								
							}
							
							if(sql == null)
								sql = "insert into " + tb_name + " values ";
							sql += "(NULL, \"" + venue + "\"," + race + ",\"" + temp_pool + "\",\""
									+ temp_com + "\"," + temp_div + ")";
							sql += ",";
						}
					}
				}
				
				if(sql != null) {
					sql = sql.substring(0, sql.length()-1) + ";";
					DbUtil.INST.exec(sql);
					res = true;					
				}
				
			} while (loading == true);
			
		} catch (Exception e) {
			logger_.error("Exception: ", e);	
		}
		logger_.info("Finish getting result for race " + race);
		return res;
	}
	
}
