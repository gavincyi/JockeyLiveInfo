package pkg;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import com.google.gson.*;


public class WinPlaProc implements InProc {

	private static Logger logger_ = Logger.getLogger(WinPlaProc.class);
	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name, int last_time) {
		//logger_.info("Process the win and place odd data");
				
		if(inputLine.contains("OUT")) {
			JsonElement root = new JsonParser().parse(inputLine);
			inputLine = root.getAsJsonObject().get("OUT").getAsString();
			
			int total_horses = race_info.getTotalHorses();
			double win_odds[]  = new double[total_horses];
			double place_odds[] = new double[total_horses];
			/*
			String[] array = inputLine.split(";");
			String time = array[0].substring(array[0].indexOf(">")+1, array[0].indexOf("@"));

			if(Integer.parseInt(time) > last_time || last_time - Integer.parseInt(time) > 120000 ) {
				
				last_time = Integer.parseInt(time);
				for (int j = 1; j <= total_horses; j++) {
					array[j] = array[j].substring(array[j].indexOf("=") + 1);
					array[j] = array[j].substring(0, array[j].indexOf("="));
					if(array[j].equals("SCR")) {
						win_odds[j-1] = 0;
						
						if(race_info.getHorseArr().get(j-1).getAttend().equals("Y")) {
							logger_.info("Change the attend state of horse " + j + " in race " + race + " to 'N'");
							race_info.getHorseArr().get(j-1).setAttend("N");
							String sql = "UPDATE `hkjc_race_info_" + race_info.getRaceDate() + "` set attend = 'N' where race = " + race + " and horse_no = " + j;
							try {
								DbUtil.INST.exec(sql);
							} catch (Exception ex) {
								logger_.error("Exception: ", ex);
							}
						}
						
					} else
						win_odds[j-1] = Double.parseDouble(array[j]);
				
				}

				for (int j = 0; j < total_horses; j++) {
					array[j + 1 + total_horses] = array[j + 1 +total_horses]
						.substring(array[j + 1 + total_horses].indexOf("=") + 1);						
					array[j] = array[j + 1 + total_horses].substring(0,
						array[j + 1 + total_horses].indexOf("="));
					if (array[j].equals("SCR") || array[j].equals("---"))
						place_odds[j] = 0;
					else
						place_odds[j] = Double.parseDouble(array[j]);
				}
				
				String sql = "insert into " + tb_name + " values ";
				
				for (int i = 0; i < total_horses; i++) {
					String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
					Integer win_turnover = GlobalVar.INST.turnover.get(race+"WIN");
					if (null == win_turnover)
						win_turnover = 0;
					Integer pla_turnover = GlobalVar.INST.turnover.get(race+"PLA");
					if (null == pla_turnover)
						pla_turnover = 0;
					sql +=  "(NULL, \"" + datetime_str.split(" ")[0] + "\",\"" + datetime_str.split(" ")[1] + "\",\"" 
							+ time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4, time.length()) + "\","
							+ race + ",\"" + race_time + "\"," + (i+1) + "," + win_odds[i] + "," + win_turnover.intValue() 
							+ "," + place_odds[i] + "," + pla_turnover.intValue() + ")";
					if(i < total_horses-1)
						sql += ", ";
					else
						sql += ";";
				}
			
				try {
					DbUtil.INST.exec(sql);
				} catch (Exception ex) {
					logger_.error("Exception: ", ex);
				}
			}
			*/
			
			String time = inputLine.substring(inputLine.indexOf(">")+1, inputLine.indexOf("@"));
			
			if(Integer.parseInt(time) > last_time || last_time - Integer.parseInt(time) > 120000 ) {				
				last_time = Integer.parseInt(time);
				String win_odd_str = inputLine.substring(inputLine.indexOf("WIN"), inputLine.indexOf("#PLA")); // Get Win Odd String
				if(win_odd_str.contains(";")) {
					String[] array = win_odd_str.split(";");
					for (int j = 1; j <= total_horses; j++) {
						array[j] = array[j].substring(array[j].indexOf("=") + 1);
						array[j] = array[j].substring(0, array[j].indexOf("="));
						if(array[j].equals("SCR")) {
							win_odds[j-1] = 0;
							
							if(race_info.getHorseArr().get(j-1).getAttend().equals("Y")) {
								logger_.info("Change the attend state of horse " + j + " in race " + race + " to 'N'");
								race_info.getHorseArr().get(j-1).setAttend("N");
								String sql = "UPDATE `hkjc_race_info_" + race_info.getRaceDate() + "` set attend = 'N' where race = " + race + " and horse_no = " + j;
								try {
									DbUtil.INST.exec(sql);
								} catch (Exception ex) {
									logger_.error("Exception: ", ex);
								}
							}
							
						} else
							win_odds[j-1] = Double.parseDouble(array[j]);
					
					}
				} else {
					for (int j = 0; j < total_horses; j++) {
						win_odds[j] = 0;
					}
				}
				
				String pla_odd_str = inputLine.substring(inputLine.indexOf("#PLA")+5); // Get Place Odd String
				if(pla_odd_str.contains(";")) {
					String[] array = pla_odd_str.split(";");
					for (int j = 0; j < total_horses; j++) {
						array[j] = array[j].substring(array[j].indexOf("=") + 1);						
						array[j] = array[j].substring(0, array[j].indexOf("="));
						if (array[j].equals("SCR")) {
							place_odds[j] = 0;
							
							if(race_info.getHorseArr().get(j).getAttend().equals("Y")) {
								logger_.info("Change the attend state of horse " + (j+1) + " in race " + race + " to 'N'");
								race_info.getHorseArr().get(j).setAttend("N");
								String sql = "UPDATE `hkjc_race_info_" + race_info.getRaceDate() + "` set attend = 'N' where race = " + race + " and horse_no = " + (j+1);
								try {
									DbUtil.INST.exec(sql);
								} catch (Exception ex) {
									logger_.error("Exception: ", ex);
								}
							}
							
						} else
							place_odds[j] = Double.parseDouble(array[j]);
					}
				} else {
					for (int j = 0; j < total_horses; j++) {
						place_odds[j] = 0;
					}
				}
				
				String sql = "insert into " + tb_name + " values ";
				
				for (int i = 0; i < total_horses; i++) {
					String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
					Integer win_turnover = GlobalVar.INST.turnover.get(race+"WIN");
					if (null == win_turnover)
						win_turnover = 0;
					Integer pla_turnover = GlobalVar.INST.turnover.get(race+"PLA");
					if (null == pla_turnover)
						pla_turnover = 0;
					sql +=  "(NULL, \"" + datetime_str.split(" ")[0] + "\",\"" + datetime_str.split(" ")[1] + "\",\"" 
							+ time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4, time.length()) + "\","
							+ race + ",\"" + race_time + "\"," + (i+1) + "," + win_odds[i] + "," + win_turnover.intValue() 
							+ "," + place_odds[i] + "," + pla_turnover.intValue() + ")";
					if(i < total_horses-1)
						sql += ", ";
					else
						sql += ";";
				}
			
				try {
					DbUtil.INST.exec(sql);
				} catch (Exception ex) {
					logger_.error("Exception: ", ex);
				}
			}
			
		}
				
		return last_time;
	}

}
