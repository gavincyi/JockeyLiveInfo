package pkg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;


public class ProcessThrd extends Thread {

	private RaceInfoItem race_info;
	private String race_date;
	private int load_wait_time;
	private int thrd_wait_time;
	private String tb_name;
	private String thrd_name;
	private String url_addr;
	private InProc processor;
	
	private static Logger logger_ = Logger.getLogger(ProcessThrd.class);

	ProcessThrd(RaceInfoItem race_info, int load_wait_time, int thrd_wait_time, String tb_name, String race_date, String thrd_name,
			 String url_addr, InProc processor){
		this.race_info = race_info;
		this.race_date = race_date;
		this.load_wait_time = load_wait_time;
		this.thrd_wait_time = thrd_wait_time;
		this.tb_name = tb_name;
		this.thrd_name = thrd_name;
		this.url_addr = url_addr;
		this.processor = processor;
		logger_.info("Creating " + thrd_name + " thread");
	}
	
	public void run() {

			logger_.info("Running " + thrd_name + " thread");
			
			int last_time = 0;
					
			while (true) {				
										
				try {
						
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.MINUTE, -30);
					Date curr = calendar.getTime();
					String ori_time = race_info.getRaceTime();
					Date dt = new SimpleDateFormat("yyyyMMdd HH:mm").parse(race_date + " " + ori_time);
					if (curr.after(dt)) {
						logger_.info(thrd_name + " stopped");
						break;											
					}
	
					boolean loading = false;
					do {
						logger_.info("fetch " + thrd_name);
						loading = false;

						List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
						for (String inputLine : line_arr) {
	
							if (inputLine.contains("Loading.gif")) {
								logger_.info("the " + thrd_name + " page " + " is still loading");
								loading = true;
								break;
							}
	
							last_time = processor.procRec(inputLine, race_info, race_info.getRaceNo(), ori_time, tb_name, last_time);
							
							if(inputLine.contains("tableContent2")) 
								break;
						}
					} while (loading == true);

				} catch (Exception e) {					
					logger_.error("Exception:", e);
					try {
						Thread.sleep(thrd_wait_time);
					} catch (InterruptedException ie) {
						logger_.error(thrd_name + " thread interrupted.", ie);
					}
				} 					
		
				try {
					Thread.sleep(thrd_wait_time);
				} catch (InterruptedException e) {
					logger_.error( thrd_name + "thread interrupted.", e);
				}
			}

		logger_.info(thrd_name + " thread exiting.");
	}	
}
