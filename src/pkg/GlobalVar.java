package pkg;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum GlobalVar {
	INST;

	public Map<String, Integer> turnover = new ConcurrentHashMap<String, Integer>();
}
