package pkg;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class TriProc implements InProc {

	private static Logger logger_ = Logger.getLogger(TriProc.class);
	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name, int last_time) {
		
		if(inputLine.contains(";")) {
			
			String time = inputLine.substring(inputLine.indexOf(">")+1, inputLine.indexOf("@"));
			if(Integer.parseInt(time) > last_time || last_time - Integer.parseInt(time) > 120000 ) {
			
				last_time = Integer.parseInt(time);
			
				
				String arr[] = inputLine
						.substring(inputLine.indexOf(";") + 1, inputLine.lastIndexOf("<")).split(";");
				String sql = "insert into " + tb_name + " values";
				for (int i = 0; i < arr.length; i++) {
					int pair[] = new int[3];
					String pair_str[] = arr[i].substring(0, arr[i].indexOf("=")).split("-");
					for (int j = 0; j < pair_str.length; j++) {
						pair[j] = Integer.parseInt(pair_str[j]);
					}
					double odd;
					if(arr[i].contains("SCR"))
						odd = 0;
					else
						odd = Double.parseDouble(
							arr[i].substring(arr[i].indexOf("=") + 1, arr[i].lastIndexOf("=")));
					String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
					Integer turnover = GlobalVar.INST.turnover.get(race+"TRI");
					if (null == turnover)
						turnover = 0;
					
					if(sql == null)
						sql = "insert into " + tb_name + " values";
					
					sql += "(NULL, \"" + datetime_str.split(" ")[0] + "\",\"" + datetime_str.split(" ")[1] + "\",\"" 
							+ time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4, time.length()) + "\","
							+ race + ",\"" + race_time + "\"," + pair[0] + "," + pair[1] + "," + pair[2] + "," + odd 
							+ "," + turnover.intValue() + ")";
	
					if(sql.length() > 2000) {
						sql += ";";
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
						sql = null;
					} else {
						if (i == arr.length - 1)
							sql += "; ";
						else
							sql += ", ";
					}
				}
				
				if(sql != null) {
					try {
						DbUtil.INST.exec(sql);
					} catch (Exception ex) {
						logger_.error("Exception: ", ex);
					} 
				}
			}
		}
		return last_time;
	}

}
