package pkg;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class TestFrDiv {

	private static final String LOG4J_CONFIG_FILE = "log4j.properties";
	private static Logger logger_ = Logger.getLogger(TestFrDiv.class);
	
	private static Properties properties_ = new Properties();

	public static Properties getProperties() {
		return properties_;
	}
	
	public static void main(String[] args) {
		try {
			PropertyConfigurator.configure(LOG4J_CONFIG_FILE);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}


		try {
			PropertyConfigurator.configure(LOG4J_CONFIG_FILE);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		try {
									
			properties_.load(new FileInputStream("config.properties"));
			logger_.info("TestFrDiv start");
			DbUtil.INST.init();
			
			final int load_wait_time = 60*1000;		
			
			update_overseas_div_result("20170408", "S1", load_wait_time, 6, "hkjc_dividend_fr_20170408");
			logger_.info("TestFrDiv Finished");
		} catch (Exception e) {
			logger_.error("Error: ", e);
		}
	}

	
	public static boolean update_overseas_div_result(String race_date, String venue, int load_wait_time, int race, String tb_name) {
		logger_.info("update overseas dividend result for race " + race);
		boolean res = false;
		try {
			boolean	loading = false;
			do {							            
				//String url_addr = "http://racing.hkjc.com/racing/info/meeting/ResultsAll/English/Local/" + race_date;
				String url_addr = "http://racing.hkjc.com/racing/SystemDataPage/racing/ResultsAll-iframe-SystemDataPage.aspx?match_id=" + race_date + "&lang=English";
				logger_.info(url_addr);
				List<String> line_arr = URLConn.load_url(url_addr, load_wait_time);
				loading = false;
				String sql = null;
				int state = 0;
				
				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}
					
					if(state == 2)
						break;
	
					if(state == 0 && inputLine.contains("Overseas ") ) {
						String pre_venue = inputLine.substring(inputLine.indexOf("(")+1, inputLine.indexOf(")"));
						int temp_race = Integer.parseInt(inputLine.substring(inputLine.indexOf("Race ")+5, inputLine.indexOf(" </div>")));
						if( temp_race ==  race && pre_venue.equals(venue))
							state = 1;
					} else if(state == 1 && inputLine.contains("REFUND")) {
						String cols[] = inputLine.split("</td>");
						String temp_pool = cols[0].substring(cols[0].indexOf(">")+2);
						String temp_com = cols[1].substring(cols[0].indexOf(">")+1);
						String temp_div = "0";
						if(sql == null)
							sql = "insert into " + tb_name + " values ";
						sql += "(NULL, \"" + venue + "\"," + race + ",\"" + temp_pool + "\",\""
								+ temp_com + "\"," + temp_div + "), ";
					} else if(state == 1 && (inputLine.contains("WIN") || inputLine.contains("</table>"))) {
						inputLine = inputLine.substring(inputLine.indexOf("<tr"));
						if(inputLine.contains("</table")) {
							inputLine = inputLine.substring(0, inputLine.indexOf("</table>"));
							state = 2;
						} else {
							inputLine = inputLine.substring(0, inputLine.lastIndexOf("</tr>"));
						}
						
						String row[] = inputLine.split("</tr>");
						String temp_pool = null;
						String temp_com = null;
						String temp_div = null;
						for (int i = 0; i < row.length; i++) {
							String cols[] = row[i].split("</td>");
							for(int j = 0; j < cols.length; j++) {
								if(cols[j].contains("rowspan")) {
									if(cols[j].contains("</br>")) {
										cols[j] = cols[j].substring(0, cols[j].indexOf("</br>")) + cols[j].substring(cols[j].indexOf("</br>")+5);
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+2);
									} else if (cols[j].contains("<BR/>")) {
										cols[j] = cols[j].substring(0, cols[j].indexOf("<BR/>")) + " " + cols[j].substring(cols[j].indexOf("<BR/>")+6);
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+1);
									} else {
										temp_pool = cols[j].substring(cols[j].lastIndexOf(">")+2);
									}
								} else if(cols[j].contains("tdAlignR")) {
									temp_div = cols[j].substring(cols[j].lastIndexOf(">")+1); 
									temp_div = temp_div.replace(",", "");
									if (temp_div.contains("NOT") )
										temp_div = "0";
									if (temp_div.contains("$"))
										temp_div = Double.toString(
													Double.parseDouble(temp_div.substring(0, temp_div.indexOf("/")))
														* Double.parseDouble(
																temp_div.substring(temp_div.indexOf("$") + 1))
												/ 10);
								} else {
									temp_com = cols[j].substring(cols[j].lastIndexOf(">")+1);
								}
								
							}
							
							if(sql == null)
								sql = "insert into " + tb_name + " values ";
							sql += "(NULL, \"" + venue + "\"," + race + ",\"" + temp_pool + "\",\""
									+ temp_com + "\"," + temp_div + ")";
							sql += ",";
						}
					}
				}
				
				if(sql != null) {
					sql = sql.substring(0, sql.length()-1) + ";";
					DbUtil.INST.exec(sql);
					res = true;					
				}
				
			} while (loading == true);
			
		} catch (Exception e) {
			logger_.error("Exception: ", e);	
		}
		logger_.info("Finish getting result for race " + race);
		return res;
	}
}
