package pkg;


public interface InProc {
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name, int last_time);
}
