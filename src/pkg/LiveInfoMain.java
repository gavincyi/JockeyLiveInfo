package pkg;

import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LiveInfoMain {

	private static final String LOG4J_CONFIG_FILE = "log4j.properties";
	private static Logger logger_ = Logger.getLogger(LiveInfoMain.class);

	private static Properties properties_ = new Properties();

	public static Properties getProperties() {
		return properties_;
	}

	public static void main(String[] args) throws Exception {
		try {
			PropertyConfigurator.configure(LOG4J_CONFIG_FILE);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		try {
			String[] date_arr = null;
			String[] venue_arr = null;

			properties_.load(new FileInputStream("config.properties"));
			logger_.info("Main start");
			DbUtil.INST.init();

			final int load_wait_time = 60 * 1000;
			final int thrd_wait_time = 5 * 1000;

			// Get race_date
			boolean loading = false;
			boolean contain_option = false;
			do {
				loading = false;
				List<String> line_arr = URLConn.load_url("http://bet.hkjc.com/racing/index.aspx?lang=en",
						load_wait_time);

				for (String inputLine : line_arr) {
					if (inputLine.contains("Loading.gif")) {
						logger_.info("the page is still loading");
						loading = true;
						break;
					}

					if (inputLine.contains("changeDate")) {
						contain_option = true;
						inputLine = inputLine.substring(inputLine.indexOf("option"),
								inputLine.lastIndexOf("</option>"));
						String[] arr = inputLine.split("</option>");
						date_arr = new String[arr.length];
						venue_arr = new String[arr.length];
						for (int i = 0; i < arr.length; i++) {
							arr[i] = arr[i].substring(arr[i].indexOf("value") + 7, arr[i].lastIndexOf("\""));
							date_arr[i] = arr[i].substring(0, 10);
							date_arr[i] = new SimpleDateFormat("yyyyMMdd")
									.format(new SimpleDateFormat("dd-MM-yyyy").parse(date_arr[i]));
							venue_arr[i] = arr[i].substring(10);
						}
					}
				}
			} while (loading == true);

			if (contain_option == false) {
				do {
					loading = false;
					List<String> line_arr = URLConn.load_url("http://bet.hkjc.com/racing/index.aspx?lang=en",
							load_wait_time);
					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (inputLine.contains("PADDING-LEFT:5px")) {
							date_arr = new String[1];
							venue_arr = new String[1];
							inputLine = inputLine.substring(inputLine.indexOf("nobr") + 5);
							date_arr[0] = inputLine.substring(0, inputLine.indexOf(","));
							date_arr[0] = new SimpleDateFormat("yyyyMMdd")
									.format(new SimpleDateFormat("dd/MM/yyyy").parse(date_arr[0]));
							inputLine = inputLine.substring(inputLine.indexOf("<nobr>") + 6,
									inputLine.lastIndexOf("</n"));
							venue_arr[0] = inputLine.substring(0, 1) + inputLine.charAt(inputLine.indexOf(" ") + 1);
							break;
						}
					}
				} while (loading == true);
			}

			Calendar cal = Calendar.getInstance();
//			cal.add(Calendar.DATE, +1); // restart program need to comment this
//										// line
			String tmr = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
			ArrayList<ArrayList<ArrayList<Thread>>> all_thrds = new ArrayList<ArrayList<ArrayList<Thread>>>();

			for (int day = 0; day < date_arr.length; day++) {
				if (Integer.parseInt(tmr) == Integer.parseInt(date_arr[day])) {
					String race_date = date_arr[day];
					String venue = venue_arr[day];
					String suffix = "";

					if (venue.matches("^.+?\\d$")) {
						String[] arr = venue.split("(?=\\d*$)", 2);
						if (Integer.parseInt(arr[1]) == 1)
							suffix = "FR_";
						else
							suffix = "FR" + arr[1] + "_";
					}

					final int number_of_tb = 11;

					String race_tb_name = "hkjc_race_info_" + suffix + race_date;
					String sql = "CREATE TABLE if not exists " + race_tb_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, region varchar(20), venue varchar(50), date varchar(10), time varchar(10), race INT, race_time varchar(10), "
							+ "race_class varchar(40), track varchar(10), course varchar(10), dist INT, going varchar(20),"
							+ "horse_no INT, horse varchar(30), draw INT, wt INT, act_wt INT, jockey varchar(30), trainer varchar(30), "
							+ "body_wt INT, rtg INT, gear varchar(20), last_run1 INT, last_run2 INT, last_run3 INT, last_run4 INT, last_run5 INT, last_run6 INT, attend varchar(5)) engine=MYISAM";
					DbUtil.INST.exec(sql);

					RaceInfoItem[] race_info_arr = getBasicInfo(load_wait_time, race_date, venue, race_tb_name);
					int total_races = race_info_arr.length;

					String win_pla_tb_name = "hkjc_win_pla_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + win_pla_tb_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), horse_no INT, win_odd DOUBLE PRECISION(10,2), "
							+ "win_inv INT, place_odd DOUBLE PRECISION(10,2), place_inv INT)";
					DbUtil.INST.exec(sql);

					String qin_tb_name = "hkjc_qin_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + qin_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, odd DOUBLE PRECISION(10,2), "
							+ "inv INT)";
					DbUtil.INST.exec(sql);

					String qpl_tb_name = "hkjc_qpl_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + qpl_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, odd DOUBLE PRECISION(10,2), "
							+ "inv INT)";
					DbUtil.INST.exec(sql);

					String tce_odd_tb_name = "hkjc_tce_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + tce_odd_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, leg3 INT, odd DOUBLE PRECISION(10,2),"
							+ "inv INT)";
					DbUtil.INST.exec(sql);

					String tce_inv_tb_name = "hkjc_tce_inv_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + tce_inv_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), horse_no INT, 1st_inv INT, 2nd_inv INT, 3rd_inv INT)";
					DbUtil.INST.exec(sql);

					String tri_tb_name = "hkjc_trio_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + tri_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, leg3 INT, odd DOUBLE PRECISION(10,2),"
							+ "inv INT)";
					DbUtil.INST.exec(sql);

					String first4_tb_name = "hkjc_first4_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + first4_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, leg3 INT, leg4 INT, "
							+ "odd DOUBLE PRECISION(10,2), inv INT)";
					DbUtil.INST.exec(sql);

					String qtt_tb_name = "hkjc_qtt_odd_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + qtt_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), leg1 INT, leg2 INT, leg3 INT, leg4 INT, "
							+ "odd DOUBLE PRECISION(10,2), inv INT)";
					DbUtil.INST.exec(sql);

					String inv_tb_name = "hkjc_inv_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + inv_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race INT, race_time varchar(10), win INT, pla INT, qin INT, qpl INT, tce INT, "
							+ "tri int, first4_n_qtt int, dbl int)";
					DbUtil.INST.exec(sql);

					String dbl_tb_name = "hkjc_dbl_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + dbl_tb_name
							+ "(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, date varchar(10), "
							+ "time varchar(10), hkjc_time varchar(10), race_time varchar(10), race1 INT, race2 INT, leg1 INT, leg2 INT, odd DOUBLE PRECISION(10,2), "
							+ "inv INT)";
					DbUtil.INST.exec(sql);

					String div_tb_name = "hkjc_dividend_" + suffix + race_date;
					sql = "CREATE TABLE if not exists " + div_tb_name
							+ " (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, venue varchar(5), race INT, pool varchar(30), win_combination varchar(50), "
							+ "dividend DOUBLE PRECISION(20,2))";
					DbUtil.INST.exec(sql);

					// ProcessThrd[][] thrds = new
					// ProcessThrd[total_races][number_of_tb];

					String date_str = new SimpleDateFormat("yyyy-MM-dd")
							.format(new SimpleDateFormat("yyyyMMdd").parse(race_date));
					String[] prefix_url_arr = {
							"http://bet.hkjc.com/racing/index.aspx?lang=en&date=" + date_str + "&venue=" + venue
									+ "&raceno=",
							"http://bet.hkjc.com/racing/getJSON.aspx?type=winplaodds&date=" + date_str
									+ "&venue=" + venue + "&start=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_full&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_full&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_banker&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_inv&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_full&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_full&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_banker&date=" + date_str + "&venue="
									+ venue + "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=pooltot&date=" + date_str + "&venue=" + venue
									+ "&raceno=",
							"http://bet.hkjc.com/racing/getXML.aspx?type=jcbwracing_full&pool=dbl&date=" + date_str
									+ "&venue=" + venue + "&raceno=" };

					String[] suffix_url_arr = { "", "&end=", "&pool=qin&tag=QIN/RACE", "&pool=qpl&tag=QPL/RACE",
							"&pool=tcebank&tag=TCE_BANK/RACE", "&pool=tceinv&tag=TCE_INV/RACE",
							"&pool=trio&tag=TRI_FULL/RACE", "&pool=ff&tag=FIRST4FULL/RACE",
							"&pool=qttbank&tag=QTT_BANK/RACE", "", "&tag=DBL/RACE" };
					String[] tb_name_arr = { race_tb_name, win_pla_tb_name, qin_tb_name, qpl_tb_name, tce_odd_tb_name,
							tce_inv_tb_name, tri_tb_name, first4_tb_name, qtt_tb_name, inv_tb_name, dbl_tb_name };
					String[] thrd_name_arr = { "update_race_time ", "win_place_odd ", "qin_odd ", "qpl_odd ",
							"tierce_odd ", "tierce_inv ", "trio_odd ", "first4_odd ", "quantet_odd ", "inv ",
							"dbl_odd " };
					InProc[] processor_arr = { new UptRaceTime(), new WinPlaProc(), new QinProc(), new QplProc(),
							new TceOddProc(), new TceInvProc(), new TriProc(), new First4Proc(), new QttProc(),
							new InvProc(), new DblProc() };

					ArrayList<ArrayList<Thread>> thrds_list = new ArrayList<ArrayList<Thread>>();

					for (int race = 1; race <= total_races; race++) {
						ArrayList<Thread> thrds = new ArrayList<Thread>();
						for (int i = 0; i < number_of_tb; i++) {
							String url_addr = prefix_url_arr[i] + race + suffix_url_arr[i];
							if (suffix_url_arr[i].contains("end"))
								url_addr += race;

							if (!(race == total_races && i == number_of_tb - 1)) {
								ProcessThrd temp_thrd = new ProcessThrd(race_info_arr[race - 1], load_wait_time,
										thrd_wait_time, tb_name_arr[i], race_date, thrd_name_arr[i] + race, url_addr,
										processor_arr[i]);
								temp_thrd.start();
								thrds.add(temp_thrd);
							} else {
								DivThrd temp_thrd = new DivThrd(race_info_arr, load_wait_time, thrd_wait_time,
										div_tb_name, race_date, "div", venue);
								temp_thrd.start();
								thrds.add(temp_thrd);
							}
						}
						thrds_list.add(thrds);
					}
					all_thrds.add(thrds_list);
				}
			}

			for (int day = 0; day < all_thrds.size(); day++) {
				ArrayList<ArrayList<Thread>> thrds_list = all_thrds.get(day);
				for (int race = 1; race <= thrds_list.size(); race++) {
					ArrayList<Thread> thrds = thrds_list.get(race - 1);
					for (int i = 0; i < thrds.size(); i++) {
						thrds.get(i).join();
					}
				}
			}
		} catch (Exception e) {
			logger_.error("Exception:", e);
		} finally {
			DbUtil.INST.free();
			logger_.info("stopped");
		}
	}

	public static RaceInfoItem[] getBasicInfo(int load_wait_time, String race_date, String venue, String tb_name) {
		String basic_url = "http://bet.hkjc.com/racing/index.aspx?lang=en&";
		RaceInfoItem[] race_info_list = null;
		int total_races = 1;
		String region = null;

		// Get horse information
		for (int race = 1; race <= total_races; race++) {
			boolean loading = false;
			try {
				do {
					logger_.info("fetching horse information for race: " + race);

					logger_.info(basic_url + "date="
							+ new SimpleDateFormat("dd-MM-yyyy")
									.format(new SimpleDateFormat("yyyyMMdd").parse(race_date))
							+ "&venue=" + venue + "&raceno=" + race);

					List<String> line_arr = URLConn.load_url(basic_url + "date="
							+ new SimpleDateFormat("dd-MM-yyyy")
									.format(new SimpleDateFormat("yyyyMMdd").parse(race_date))
							+ "&venue=" + venue + "&raceno=" + race, load_wait_time);
					loading = false;

					String recs[][] = null;
					String attend_arr[] = null;
					int total_horses = 1;
					int wt_increment[] = null;
					int last_6_run_arr[][];
					List<HorseInfoItem> horse_info_list = new ArrayList<HorseInfoItem>();

					for (String inputLine : line_arr) {
						if (inputLine.contains("Loading.gif")) {
							logger_.info("the page is still loading");
							loading = true;
							break;
						}

						if (race == 1 && inputLine.contains("race_num_")) {
							inputLine = inputLine.substring(inputLine.lastIndexOf("race_num_") + 9);
							total_races = Integer.parseInt(inputLine.substring(0, inputLine.indexOf("\"")));
							race_info_list = new RaceInfoItem[total_races];
						}

						if (race == 1 && inputLine.contains("PADDING-LEFT:5px")) {
							inputLine = inputLine.substring(inputLine.lastIndexOf("<nobr>") + 6,
									inputLine.lastIndexOf("</nobr>"));
							region = inputLine;
							if (region.equals("Sha Tin") || region.equals("Happy Valley"))
								region = "HK";
						}

						// get race info
						if (inputLine.contains("class=\"content\" nowrap")) {
							String arr[] = inputLine.split("</nobr>");
							String race_time = arr[1].substring(arr[1].indexOf(">") + 1);
							String race_class = arr[2].substring(arr[2].indexOf(">") + 1);

							String track = arr[3].substring(arr[3].indexOf(">") + 1);
							if (track.contains("Track"))
								track = "awt";
							else
								track = track.substring(0, 1).toLowerCase() + track.substring(1);

							String course = "";
							if (arr.length > 6)
								course = arr[4].substring(arr[4].indexOf("\"") + 1, arr[4].lastIndexOf("\""));

							int dist = Integer.parseInt(arr[arr.length - 2]
									.substring(arr[arr.length - 2].indexOf(">") + 1, arr[arr.length - 2].indexOf("m")));

							String going = "";
							if (arr[arr.length - 1].contains(",")) {
								going = arr[arr.length - 1].substring(2, arr[arr.length - 1].indexOf("<"));
							}

							race_info_list[race - 1] = new RaceInfoItem(race, race_date, race_time, race_class, track,
									course, dist, going);
						}

						// get horse info
						if (inputLine.contains("tableContent2")) {

							String arr[] = inputLine.split("</tr>");
							total_horses = arr.length;
							last_6_run_arr = new int[total_horses][6];
							recs = new String[total_horses][12];
							attend_arr = new String[total_horses];
							wt_increment = new int[total_horses];

							for (int i = 0; i < total_horses; i++) {
								String cols[] = arr[i].split("</td>");

								if (cols.length < 10) {
									for (int j = 1; j < cols.length - 1; j++) {
										recs[i][j] = cols[j].substring(cols[j].lastIndexOf("\">") + 2);
										if (j == 3)
											recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf("<"));
									}
									for (int j = 4; j < 12; j++)
										recs[i][j] = "0";
									attend_arr[i] = "N";

								} else {

									for (int j = 1; j < cols.length; j++) {
										recs[i][j] = cols[j].substring(cols[j].lastIndexOf("\">") + 2);
										if (j == 6 && recs[i][j].contains("(")) {
											// wt_increment[i] =
											// Integer.parseInt(recs[i][j].substring(recs[i][j].indexOf("(")+1,
											// recs[i][j].indexOf(")")).replace("?",
											// ""));
											String wt_change = recs[i][j]
													.substring(recs[i][j].indexOf("(") + 1, recs[i][j].indexOf(")"))
													.replace("?", "");
											if (wt_change.matches("^[+-]?\\d+$")) {
												wt_increment[i] = Integer.parseInt(wt_change);
											}
										} else if (j == 6)
											wt_increment[i] = 0;
										if (j == 3 || j == 6 || j == 7) {
											recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf("<"));
										}

										if (j == 6 && recs[i][j].contains("(")) {
											recs[i][j] = recs[i][j].substring(0, recs[i][j].indexOf(" ("));
										}

										if (j == 8) {
											recs[i][j] = recs[i][j].replace("&nbsp;", "0");
										}

										if (j == 9 && !recs[i][j].matches("[0-9]+"))
											recs[i][j] = "0";

										if (j == 10) {
											if (recs[i][j].contains("&nbsp;"))
												recs[i][j] = "";
											else
												recs[i][j] = recs[i][j].substring(recs[i][j].indexOf(">") + 1,
														recs[i][j].indexOf("</"));
										}

										if (j == 11) {
											String[] runs = recs[i][j].split("/");
											int k;
											for (k = 0; k < runs.length; k++) {
												if (runs[k].contains("&nbsp;") || runs[k].contains("-"))
													last_6_run_arr[i][k] = 0;
												else
													last_6_run_arr[i][k] = Integer.parseInt(runs[k]);
											}

											for (; k < 6; k++) {
												last_6_run_arr[i][k] = 0;
											}
										}

									}
									attend_arr[i] = "Y";
								}
							}

							String sql = "insert into " + tb_name + " values ";
							for (int i = 0; i < total_horses; i++) {
								horse_info_list.add(new HorseInfoItem(Integer.parseInt(recs[i][1]), recs[i][3],
										Integer.parseInt(recs[i][4]), Integer.parseInt(recs[i][5]),
										Integer.parseInt(recs[i][5]) + wt_increment[i], recs[i][6], recs[i][7],
										Integer.parseInt(recs[i][8]), Integer.parseInt(recs[i][9]), recs[i][10],
										last_6_run_arr[i], attend_arr[i]));

								String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
								int[] run_arr = horse_info_list.get(i).getLast6Run();
								sql += "(NULL, \"" + region + "\",\"" + venue + "\", \"" + datetime_str.split(" ")[0]
										+ "\",\"" + datetime_str.split(" ")[1] + "\"," + race + ",\""
										+ race_info_list[race - 1].getRaceTime() + "\",\""
										+ race_info_list[race - 1].getClassNo() + "\",\""
										+ race_info_list[race - 1].getTrack() + "\",\""
										+ race_info_list[race - 1].getCourse() + "\","
										+ race_info_list[race - 1].getDist() + ",\""
										+ race_info_list[race - 1].getGoing() + "\","
										+ horse_info_list.get(i).getHorseNo() + ",\""
										+ horse_info_list.get(i).getHorse() + "\"," + horse_info_list.get(i).getDraw()
										+ "," + horse_info_list.get(i).getWgt() + ","
										+ horse_info_list.get(i).getActWgt() + ",\""
										+ horse_info_list.get(i).getJockey() + "\",\""
										+ horse_info_list.get(i).getTrainer() + "\","
										+ horse_info_list.get(i).getBodyWgt() + "," + horse_info_list.get(i).getRtg()
										+ ",\"" + horse_info_list.get(i).getGear() + "\", ";

								for (int j = 0; j < 6; j++)
									sql += run_arr[j] + ", ";
								sql += "\"" + horse_info_list.get(i).getAttend() + "\")";

								if (i < total_horses - 1)
									sql += ",";
								else
									sql += ";";

							}
							try {
								DbUtil.INST.exec(sql);
							} catch (Exception ex) {
								logger_.error("Exception: ", ex);
							}
							race_info_list[race - 1].setHorse(total_horses, horse_info_list);

							break;
						}

					}
				} while (loading == true);

			} catch (ParseException e) {
				logger_.error("ParseException:", e);
			} catch (Exception e) {
				logger_.error("Exception:", e);
			}
		}
		return race_info_list;
	}
}
