package pkg;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class TceInvProc implements InProc {

	private static Logger logger_ = Logger.getLogger(TceInvProc.class);
	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name, int last_time) {
		
		if(inputLine.contains(";")) {
			
			String time = inputLine.substring(inputLine.indexOf(">")+1, inputLine.indexOf("@"));
			if(Integer.parseInt(time) > last_time || last_time - Integer.parseInt(time) > 120000 ) {
			
				last_time = Integer.parseInt(time);
				
				String arr[] = inputLine.substring(inputLine.indexOf(";")+1, inputLine.lastIndexOf("<")).split(";");
				
				String sql = "insert into " + tb_name + " values";
				for(int i = 0; i < arr.length; i++) {
					String cols[] = arr[i].substring(arr[i].indexOf("|")+1).split("\\|");
					int tce_inv_arr[] = new int[3];
					for(int j = 0; j < cols.length; j++) {		
						if(cols[j].contains("SCR") || cols[j].contains("---"))
							tce_inv_arr[j] = 0;
						else
							tce_inv_arr[j] = Integer.parseInt(cols[j].substring(cols[j].indexOf("=")+1));
					}
					String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
					sql += "(NULL, \"" + datetime_str.split(" ")[0] + "\",\""+ datetime_str.split(" ")[1] + "\",\"" 
							+ time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4, time.length()) + "\","
							+ race + ",\"" + race_time + "\"," + (i+1) + "," + tce_inv_arr[0] + "," + tce_inv_arr[1] + ","
							+ tce_inv_arr[2] + ")";
							
					if(i == arr.length-1)
						sql += "; ";
					else
						sql += ", ";
				}
				
				
				try {
					DbUtil.INST.exec(sql);
				} catch (Exception ex) {
					logger_.error("Exception: ", ex);
				}
			}
		}
		return last_time;
	}

}
