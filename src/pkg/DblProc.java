package pkg;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class DblProc implements InProc {

	private static Logger logger_ = Logger.getLogger(DblProc.class);

	@Override
	public int procRec(String inputLine, RaceInfoItem race_info, int race, String race_time, String tb_name,
			int last_time) {
		if (inputLine.contains(";")) {
			String time = inputLine.substring(inputLine.indexOf(">") + 1, inputLine.indexOf("@"));
			if (Integer.parseInt(time) > last_time || last_time - Integer.parseInt(time) > 120000) {
				last_time = Integer.parseInt(time);
				String input_arr[] = inputLine.substring(inputLine.indexOf(";") + 1, inputLine.indexOf("</OUT>"))
						.split(";");

				String sql = "insert into " + tb_name + " values ";
				for (int i = 0; i < input_arr.length; i++) {
					String temp_Dbl = input_arr[i].substring(input_arr[i].indexOf("=") + 1,
							input_arr[i].lastIndexOf("="));
					if (temp_Dbl.equals("SCR"))
						temp_Dbl = "0";
					int horse1 = Integer.parseInt(input_arr[i].substring(0, input_arr[i].indexOf("-")));
					int horse2 = Integer
							.parseInt(input_arr[i].substring(input_arr[i].indexOf("-") + 1, input_arr[i].indexOf("=")));

					String datetime_str = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date());
					Integer turnover = GlobalVar.INST.turnover.get(race + "DBL");
					if (null == turnover)
						turnover = 0;

					if (sql == null)
						sql = "insert into " + tb_name + " values ";

					sql += "(NULL, \"" + datetime_str.split(" ")[0] + "\",\"" + datetime_str.split(" ")[1] + "\",\""
							+ time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, time.length())
							+ "\",\"" + race_time + "\"," + race + "," + (race + 1) + "," + horse1 + "," + horse2 + ","
							+ Double.parseDouble(temp_Dbl) + "," + turnover + ")";

					if (sql.length() > 2000) {
						sql += ";";
						try {
							DbUtil.INST.exec(sql);
						} catch (Exception ex) {
							logger_.error("Exception: ", ex);
						}
						sql = null;
					} else {

						if (i < input_arr.length - 1)
							sql += ", ";
						else
							sql += ";";
					}
				}

				if (sql != null) {
					try {
						DbUtil.INST.exec(sql);
					} catch (Exception ex) {
						logger_.error("Exception: ", ex);
					}
				}

			}
		}
		return last_time;
	}
}
